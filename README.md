


Todo-List-App

```
На хосте установлено:
- ansible [core 2.16]   
- vagrant 2.4.1   
- virtualBox 7.0    
```


В папке "vagrant" файлы для vagrant и для ansible.   
Когда запускаем Vagrant, автоматичемки установятся ансибл роли, для каждой группы свои.
Группы машин:   
 - gr_servers - это prod окружение 
 - gr_clients - это dev окружение 

  
[Vagrantfile](https://gitlab.com/lessons_devops/240322/control_ci_cd/-/blob/main/vagrant/Vagrantfile)    

На хост прокидывается порт 80 (для prod окружения)   
"forwarded_port", guest: 80, host: 80

На хост прокидывается порт 8777   (для dev окружения)  
"forwarded_port", guest: 80, host: 8777
	
Ansible:
- [playbook_clients.yml](https://gitlab.com/lessons_devops/240322/control_ci_cd/-/blob/main/vagrant/playbook_clients.yml)

- [playbook_servers.yml](https://gitlab.com/lessons_devops/240322/control_ci_cd/-/blob/main/vagrant/playbook_servers.yml)

- [requirements.yml](https://gitlab.com/lessons_devops/240322/control_ci_cd/-/blob/main/vagrant/requirements.yml)


Ansible roles:  
- [Gitlab_runner](https://gitlab.com/library585605/ansible_roles/gitlab_runner_install)
- [Docker](https://gitlab.com/library585605/ansible_roles/mysql)
- [MySQL](https://gitlab.com/library585605/ansible_roles/mysql)

---

GitLab-CI:
Один общий файл [gitlab_flows](https://gitlab.com/lessons_devops/240322/gitlab_flows/-/blob/main/gitlabci.yml)  для всех веток. Подключаем его в каждой ветки в файле [.gitlab-ci.yml](https://gitlab.com/lessons_devops/240322/control_ci_cd/-/blob/main/.gitlab-ci.yml)
   
```
Branch:  
 - main
 - develop  
 - feature*     
```
 

[Dockerfile](https://gitlab.com/lessons_devops/240322/control_ci_cd/-/blob/main/docker/dockerfile) - везде одинаковый.   (Уже подконец проекта понял, что его тоже нужно в отделный репозиторий, вместе nginx-proxy и подключить в проект)

В develop и feature* ветках есть папка с [nginx-proxy](https://gitlab.com/lessons_devops/240322/control_ci_cd/-/blob/develop/docker/nginx-proxy/docker-compose.yml) для запуска или остановки прокси. 

В ветке develop файл [docker-compose.yml](https://gitlab.com/lessons_devops/240322/control_ci_cd/-/blob/develop/docker/docker-compose.yml) отличается, здесь указана статическая папка для БД и статические название контейнеров. 

 
В ветках feature* шаблонный файл [docker-compose.template.yml](https://gitlab.com/lessons_devops/240322/control_ci_cd/-/blob/feature-sales/docker/docker-compose.template.yml)   изменяется название контейнера, сервисов и название хранилищ для тестовых БД (добавляется к названию обьектов хеш комита) ${CI_COMMIT_SHORT_SHA}

	Nginx-Proxy
Для работы nginx-proxy нужен DNS-сервер. 
Но в место него использовал локально файл hosts.
Для данного проекта А-записи нужно вносить вручную на хост машине.   
Пример:    
  127.0.0.1       example.com   
  127.0.0.1       dev.example.com   
  127.0.0.1       3a05687c.dev.example.com   
  127.0.0.1       ec01e7c9.dev.example.com   
 
---
Образы приложения в репозиториях:   
- Hub-docker  
Images: [todo-list](https://hub.docker.com/r/ganebaldenis/todo_list_app/tags)

- Gitlab  
Container_registry: [todo-list](https://gitlab.com/lessons_devops/240322/control_ci_cd/container_registry/6366796)


Screenshots:



![image info](vagrant/screenshots/ec01e7c9.dev%20pipeline.png)
  
![image info](vagrant/screenshots/Pipelines-dev.png)

![image info](vagrant/screenshots/docker%20ps%20dev.png)

![image info](vagrant/screenshots/site_prod.png)

![image info](vagrant/screenshots/site%20dev.png)

![image info](vagrant/screenshots/site%203a05687c.dev.png)

![image info](vagrant/screenshots/site%20ec01e7c9.dev.png)
